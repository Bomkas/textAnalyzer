import React from 'react';
import {Button} from 'react-bootstrap';
import FieldGroup from "../../components/FieldGroup/FieldGroup";

class MainContainer extends React.Component {
  constructor(props) {
    super(props);
    this.handleFileChange = this.handleFileChange.bind(this);
    this.countTheWords = this.countTheWords.bind(this);
    this.checkBrackets = this.checkBrackets.bind(this);
    this.state = {
      selectedFile: ''
    };
  }

  handleFileChange(e) {
    e.preventDefault();

    let reader = new FileReader();
    let file = e.target.files[0];

    reader.onloadend = () => {
      this.setState({
        selectedFile: file
      })
    };

    reader.readAsDataURL(file);
  }

  countTheWords() {
    const {selectedFile} = this.state;

    const formData = new FormData();
    formData.append('file', selectedFile);

    fetch("/count", {
      method: 'POST',
      body: formData
    }).then(function (msg) {
      return msg.text()
    }).then(function (text) {
      let out = "";
      text.slice(0, -1).split("|").forEach(function (item) {
          out += (item) += ("\n");
        }
      );
      alert(out);
    }).catch(function (e) {
      alert("Error: " + e)
    })
  }

  checkBrackets() {
    const {selectedFile} = this.state;

    const formData = new FormData();
    formData.append('file', selectedFile);

    fetch("/brackets", {
      method: "POST",
      body: formData
    }).then(function (msg) {
      return msg.text()
    }).then(function (text) {
      alert(text);
    }).catch(function (e) {
      alert("Error: " + e)
    })
  }

  render() {
    return (
      <div className="text-analyzer-main-box">
        <div className="text-analyzer-frame">
          <p className="text-analyzer-text-header">Text Analyzer</p>
          <div className="text-analyzer-interior-frame">
            <FieldGroup
              id="formControlsFile"
              type="file"
              onChange={(e) => this.handleFileChange(e)}
            />
            <Button title="Displays the top 10 repeating words"
                    bsClass="react-bootstrap-btn text-analyzer-right-bottom-button"
                    onClick={this.countTheWords}>
              Count the words</Button>
            <Button title="Checking the correctness of the brackets"
                    bsClass="react-bootstrap-btn text-analyzer-left-bottom-button"
                    onClick={this.checkBrackets}>
              Check brackets</Button>
          </div>
        </div>
      </div>
    )
  }
}

export default MainContainer;