package com.textanalyzer.app.controller;

import com.textanalyzer.app.components.CheckBrackets;
import com.textanalyzer.app.components.CountTheWords;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;

@Controller
public class HomeController {

  @RequestMapping(value = "/count", method = RequestMethod.POST, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
  public @ResponseBody
  String handleFileUpload(@RequestParam("file") MultipartFile file) {
    if (!file.isEmpty()) {
      String name = file.getName();
      try {
        File newFile = saveFile(name, file);

        HashMap<String, Integer> map = CountTheWords.result(name);
        StringBuilder out = new StringBuilder();
        for (int i = 0; i < 10; i++) {
          HashMap.Entry<String, Integer> maxEntry = map.entrySet().stream().max(HashMap.Entry.comparingByValue()).get();

          out.append(maxEntry.getValue()).append(" ").append(maxEntry.getKey()).append("|");

          map.remove(maxEntry.getKey());
        }

        newFile.delete();
        return out.toString();
      } catch (Exception e) {
        return "not ok";
      }
    } else return "Файл пуст или имеет неверный формат";
  }

  @RequestMapping(value = "/brackets", method = RequestMethod.POST, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
  public @ResponseBody
  String checkBrackets(@RequestParam("file") MultipartFile file) {
    if (!file.isEmpty()) {
      String name = file.getName();
      try {
        saveFile(name, file);

        return CheckBrackets.result(name);
      } catch (Exception e) {
        return "not ok";
      }
    } else return "Файл пуст или имеет неверный формат";
  }

  private File saveFile(String name, MultipartFile file) throws IOException {
    File newFile = new File(name);
    byte[] bytes = file.getBytes();
    BufferedOutputStream stream =
      new BufferedOutputStream(new FileOutputStream(newFile));
    stream.write(bytes);
    stream.close();

    return newFile;
  }

}
