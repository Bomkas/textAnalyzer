package com.textanalyzer.app.components;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public abstract class CountTheWords {
  private static final String path = "exceptions/";
  private static List<String> pretext;
  private static List<String> pronouns;
  private static List<String> union;

  static {
    try {
      pretext = Utility.getLines(path + "union.txt");
      pronouns = Utility.getLines(path + "pronouns.txt");
      union = Utility.getLines(path + "union.txt");
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  public static HashMap<String, Integer> result(String path) throws IOException {

    List<String> lines = Utility.getLines(path);
    HashMap<String, Integer> top = new HashMap<>();

    for (String inLine: lines) {
      String[] lineArray = remove(getArrayLine(inLine));

      for (String word: lineArray) {
        if (!top.containsKey(word)) {
          top.put(word, 1);
        } else {
          Integer value = top.get(word);
          top.put(word, value + 1);
        }
      }
    }

    return top;
  }

  private static String[] getArrayLine(String line) {
    return line.toLowerCase().replaceAll("[^а-яa-z]+", " ")
      .split(" +");
  }

  private static String[] remove(String[] array) {
    List<String> list = new ArrayList<>();

    for (String anArray: array) {
      if (!pretext.contains(anArray) && !pronouns.contains(anArray) && !union.contains(anArray))
        list.add(anArray);
    }

    return list.toArray(new String[0]);
  }
}
