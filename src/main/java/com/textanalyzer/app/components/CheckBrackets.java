package com.textanalyzer.app.components;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Stack;

public abstract class CheckBrackets {
  public static String result(String path) throws IOException {
    List<String> lines = Utility.getLines(path);
    HashMap<String, Integer> openBrackets = new HashMap<String, Integer>() {{
      put("{", 0);
      put("[", 1);
      put("(", 2);
    }};
    HashMap<String, Integer> closeBrackets = new HashMap<String, Integer>() {{
      put("}", 0);
      put("]", 1);
      put(")", 2);
    }};
    Stack<String> stack = new Stack<>();
    String result = "correct";

    for (String inLine: lines) {
      for (int i = 0; i < inLine.length(); i++) {
        Character character = inLine.charAt(i);
        String symbol = character.toString();

        if (openBrackets.containsKey(symbol)) {
          stack.push(symbol);
        } else if (closeBrackets.containsKey(symbol)) {
          if (stack.empty() || !openBrackets.get(stack.pop()).equals(closeBrackets.get(symbol))) {
            result = "incorrect";
            break;
          }
        }
      }
    }

    return result;
  }
}
