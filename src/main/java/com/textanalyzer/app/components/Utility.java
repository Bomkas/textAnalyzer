package com.textanalyzer.app.components;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

abstract class Utility {
  static List<String> getLines(String path) throws IOException {
    BufferedReader reader = new BufferedReader(new FileReader(path));
    String line;
    List<String> lines = new ArrayList<>();
    while ((line = reader.readLine()) != null) {
      lines.add(line);
    }
    reader.close();

    return lines;
  }
}
