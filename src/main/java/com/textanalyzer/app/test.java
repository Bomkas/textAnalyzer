package com.textanalyzer.app;


import com.textanalyzer.app.components.CheckBrackets;
import com.textanalyzer.app.components.CountTheWords;

import java.io.IOException;
import java.util.HashMap;

class test {
  public static void main(String[] args) throws IOException {

    HashMap<String, Integer> map = CountTheWords.result("test.txt");

    for (int i = 0; i < 10; i++) {
      HashMap.Entry<String, Integer> maxEntry = map.entrySet().stream().max(HashMap.Entry.comparingByValue()).get();

      System.out.println(maxEntry.getValue() + " " + maxEntry.getKey());

      map.remove(maxEntry.getKey());
    }

    System.out.println("\n" + CheckBrackets.result("test.txt"));

  }
}
